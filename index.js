// console.log("Happy monday")

// Fetch keyword;

//Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => {
    // console.log(response.json())
        return response.json()
}).then(result => {
    console.log(result)
    showPost(result)
})

//Show post
// we are going to create a function that will let us show the posts form our API

const showPost = (posts) => {
    let entries = ``;
    posts.forEach((post)=>{
        entries += `
            <div id = "post-${post.id}">
                <h3 id = "post-title-${post.id}">${post.title}{</h3>
                <p id= "post-body-${post.id}">${post.body}</p>
                <button onclick="editPost(${post.id})">Edit</button>
                <button onclick ="deletePost(${post.id})">Delete</button>
            
            </div>
        `
    })
    document.querySelector("#div-post-entries").innerHTML = entries
}
//Post data in our API
document.querySelector('#form-add-post').addEventListener("submit", (event) =>{

    //to change the autoreload of the submit method
    event.preventDefault();


    //post method
        //if we use the post request, the fetch method will return the newly created document.
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1

        }),
        headers: {
            'Content-Type' : 'application/json'
        }
    }).then(response => response.json())
    .then(result => {
        console.log(result)



        document.querySelector("#txt-title").value = null;
        document.querySelector("#txt-body").value = null;


        alert("Post is successfully added!");

    })


})


// EDIT post

const editPost = (id) => {

    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    console.log(title)
    console.log(body)


    document.querySelector(`#txt-edit-title`).value = title
    document.querySelector(`#txt-edit-body`).value = body
    document.querySelector(`#txt-edit-id`).value = id

    document.querySelector(`#btn-submit-update`).removeAttribute('disabled')
}


document.querySelector(`#form-edit-post`)
.addEventListener("submit", (event) =>{
    event.preventDefault;

    let id =document.querySelector('#txt-edit-id').value
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: 'PUT',
        
        body: JSON.stringify ({
            title: document.querySelector("#txt-edit-title").value,
            id:  id,
            body: document.querySelectorAll("txt-edit-body").value,
            userId : 1

        }),
        headers:{
            'Content-Type' : 'application/json'
        }

    }).then(response => response.json())
    .then(result => {
        console.log(result);

    
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;
        document.querySelector("#txt-edit-id").value =null;

        alert("Post is successfully edited!");

        document.querySelector(`#btn-submit-update`).setAttribute('disabled', true)
    })
})
const deletePost = async (id) => {
    let posts = fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: 'DELETE',
        
       
    })
    document.querySelector(`#post-${id}`).remove()
}